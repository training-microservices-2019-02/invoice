package com.artivisi.training.microservice201902.invoice.dao;

import com.artivisi.training.microservice201902.invoice.entity.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CustomerDao extends PagingAndSortingRepository<Customer, String> {
}
