package com.artivisi.training.microservice201902.invoice.dao;

import com.artivisi.training.microservice201902.invoice.entity.Invoice;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface InvoiceDao extends PagingAndSortingRepository<Invoice, String> {
}
